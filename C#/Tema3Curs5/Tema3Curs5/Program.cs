﻿using System;
using System.IO;


namespace Tema3Curs5
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllLines(@"C:\Users\DC073097\source\repos\Tema3Curs5\Tema3Curs5\vectori.txt");
            int i = 0 ;
            string []s1 = input[0].Split();
            string [] s2 = input[1].Split();

            int[] v1 = new int[s1.Length];
            foreach (string nr in s1)
            {
                v1[i] = Int32.Parse(nr);
                i++;
            }

            i = 0;
            int[] v2 = new int[s2.Length];
            foreach (string nr in s2)
            {
                v2[i] = Int32.Parse(nr);
                i++;
            }

            i = 0;
            int j = 0, k = 0;
            int[] res = new int[s1.Length + s2.Length];
            while (i< s1.Length && j<s2.Length)
            {
                if(v1[i] <= v2[j])
                {
                    res[k++] = v1[i++];
                }
                else
                {
                    res[k++] = v2[j++];
                }

                
            }
            while (i < s1.Length)
            {
                res[k++] = v1[i++];
            }
            while (j < s2.Length)
            {
                res[k++] = v2[j++];
            }

            foreach (int nr in res)
            {
                Console.Write(nr + " ");

            }

        }
    }
}
