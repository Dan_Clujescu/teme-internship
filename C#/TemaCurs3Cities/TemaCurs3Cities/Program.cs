﻿using System;
using System.Linq;

namespace TemaCurs3Cities
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] cities = { "ROME", "LONDON", "AMSTERDAm" };
            var result = cities.Where(city => city.StartsWith("A", StringComparison.OrdinalIgnoreCase) 
            && city.EndsWith("M", StringComparison.OrdinalIgnoreCase));
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
            
        }
    }
}
