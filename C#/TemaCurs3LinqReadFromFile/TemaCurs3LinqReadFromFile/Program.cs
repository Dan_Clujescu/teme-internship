﻿using System;
using System.IO;
using System.Linq;

namespace TemaCurs3LinqReadFromFile
{
    class Program
    {
        static void Main(string[] args)
        {

            var animale = File.ReadAllText(@"C:\Users\DC073097\source\repos\TemaCurs3LinqReadFromFile\TemaCurs3LinqReadFromFile\animaleTest.txt")
                .Split(" ");
            var result = String.Join(", ", animale);
            Console.WriteLine(result);
        }
    }
}
