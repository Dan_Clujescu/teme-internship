﻿using System;

using AnimalLibrary;
namespace TemaCurs2RepoOfAnimals
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Calut",
                Description = "Calut frumos"
            };
            Animal t1 = new Tiger
            {
                Id = 1,
                Name = "Tigru1",
                Description = "MiauMiau mare"
            };

            AnimalRepository animale = new AnimalRepository();
            animale.Add(a1);
            animale.Add(t1);

            foreach (Animal a in animale.GetAll())
            {
                Console.WriteLine(a);
            }

        }
    }
}
