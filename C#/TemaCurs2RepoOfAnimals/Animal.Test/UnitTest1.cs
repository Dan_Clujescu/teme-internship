﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnimalLibrary;

namespace Animals.Test
{
    [TestClass]
    public class UnitTest1
    {
        Animal a1 = new Animal
        {
            Id = 1,
            Name = "Animal1",
            Description = "Primul animal"
        };

        Animal a2 = new Animal
        {
            Id = 2,
            Name = "Animal2",
            Description = "Ultimul animal"
        };

        [TestMethod]
        public void GetCorrectData_Test()
        {
            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);

            Animal primul = repo.GetById(1);
            Animal alDoilea = repo.GetById(2);

            Assert.AreEqual(a1.Name, primul.Name);
            Assert.AreEqual(a1.Description, primul.Description);
            Assert.AreEqual(a1.Id, primul.Id);
        }
    }
}
