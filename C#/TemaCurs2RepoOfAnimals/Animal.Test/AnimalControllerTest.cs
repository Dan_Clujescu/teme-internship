﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnimalLibrary;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Animals.Test
{
    [TestClass]
    public class AnimalControllerTest
    {
        

        [TestMethod]
        public void GetDataTest()
        {
            Mock<IAnimalRepo> repoMock = new Mock<IAnimalRepo>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Animal1",
                Description = "Primul animal"
            };

            repoMock.Setup(x => x.GetById(1)).Returns(a1);
            AnimalController controller = new AnimalController(repoMock.Object);
            Animal result = controller.GetAnimalById(1);

            Assert.AreEqual(a1.Id, result.Id);
            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);
        }

        [TestMethod]

        public void GetAllData_Test()
        {
            Mock<IAnimalRepo> mock = new Mock<IAnimalRepo>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Animal1",
                Description = "Primul animal"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Animal2",
                Description = "ultimul animal"
            };

            List<Animal> animals = new List<Animal>
            {
                a1,
                a2
            };

            mock.Setup(x => x.GetAll()).Returns(animals);

            AnimalController controller = new AnimalController(mock.Object);

            List<Animal> result = controller.GetAll().ToList();

            Assert.AreEqual(2, result.Count);

 
        }
    }
}
