﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalLibrary
{
    public interface IAnimalRepo
    {
        void Add(Animal a);
        void Edit(Animal a, String desc);
        void Print(Animal a);
        void Remove(Animal a);

        Animal GetById(int Id);
        IEnumerable<Animal> GetAll();

    }
}
