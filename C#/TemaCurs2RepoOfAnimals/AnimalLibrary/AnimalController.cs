﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalLibrary
{
    public class AnimalController
    {
        readonly IAnimalRepo _AnimalRepo;

        public AnimalController(IAnimalRepo animalRepo)
        {
            _AnimalRepo = animalRepo;
        }

        public AnimalController()
        {
            _AnimalRepo = new AnimalRepository();
        }

        public void Insert(Animal a)
        {
            _AnimalRepo.Add(a);
        }

        public Animal GetAnimalById(int Id)
        {
            return _AnimalRepo.GetById(Id);

        }

        public IEnumerable<Animal> GetAll() => _AnimalRepo.GetAll();


    }
}
