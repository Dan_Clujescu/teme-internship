﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalLibrary
{
    public class AnimalRepository : IAnimalRepo
    {
        readonly List<Animal> animals = new List<Animal>();

        public void Add(Animal a)
        {
            animals.Add(a);
        }
        public void Edit(Animal a, String desc)
        {
            a.Description = desc;
        }
        public void Print(Animal a)
        {
            Console.WriteLine(a);
        }
        public void Remove(Animal a)
        {
            animals.Remove(a);
        }

        public Animal GetById(int Id)
        {
            foreach(Animal a in animals)
            {
                if(a.Id == Id)
                {
                    return a;
                }
            }

            return null;
        }
        public IEnumerable<Animal> GetAll() => animals;

    }
}
