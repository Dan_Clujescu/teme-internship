﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalLibrary
{
    public class Tiger : Animal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public override string ToString() => $"Tiger Info: {Id} {Name} {Description}";
    }
}
