﻿using System;
using System.IO;

namespace Tema4Curs5
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText(@"C:\Users\DC073097\source\repos\Tema4Curs5\Tema4Curs5\TextFile1.txt");
            string [] inputStringVect = input.Split(", ");
            int[] vector = new int[inputStringVect.Length];
            int i = 0, count = 0;
            foreach(string nr in inputStringVect)
            {
                vector[i++] = Int32.Parse(nr);
            }
            for(i = 0; i< vector.Length; i++)
            {
                if(vector[i] != 0)
                {
                    vector[count++] = vector[i];
                }
            }
            while(count< vector.Length)
            {
                vector[count++] = 0;
            }

            foreach (int nr in vector)
            {
                Console.Write(nr + " ");

            }
        }
    }
}
